FROM nginx
COPY nginx.conf /etc/nginx/nginx.conf
COPY server.key /etc/ssl/private/nginx-selfsigned.key
COPY server.crt /etc/ssl/certs/nginx-selfsigned.crt
