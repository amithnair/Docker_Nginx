
# Nginx Container Without bridge just for testing(Build and run)
sudo docker build -t nginx_with_ssl . && docker run -p 8080:80 -p 8081:443  --name nginxcontainer -d nginx_with_ssl

#Create network bridge:
docker network create -d bridge ssl-network-bridge

#Run golang container
docker run --net ssl-network-bridge    -ti bluealert/hello

#Run Nginx Container
docker run  --net ssl-network-bridge  --name nginxcontainer -d nginx_with_ssl


#Fetch ssl certficate from url
openssl s_client -host facebook.com -port 443 -prexit -showcerts